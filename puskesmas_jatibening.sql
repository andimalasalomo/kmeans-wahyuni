-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 12 Agu 2018 pada 01.10
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `puskesmas_jatibening`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `akun`
--

CREATE TABLE `akun` (
  `id_akun` int(11) NOT NULL,
  `username` varchar(10) NOT NULL,
  `hash` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `akun`
--

INSERT INTO `akun` (`id_akun`, `username`, `hash`) VALUES
(1, 'admin', '$2y$10$/jowgkDhe4I9wL0T5WZkIuJW4zim72QN4ngN0O87m5z5H8lNP5tSq');

-- --------------------------------------------------------

--
-- Struktur dari tabel `daftar_penyakit`
--

CREATE TABLE `daftar_penyakit` (
  `iddaftar_penyakit` int(11) NOT NULL,
  `nama_penyakit` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `daftar_penyakit`
--

INSERT INTO `daftar_penyakit` (`iddaftar_penyakit`, `nama_penyakit`) VALUES
(1, 'Cacar air'),
(2, 'Demam paratifoid'),
(3, 'Diare dan Gastroenteritis'),
(4, 'Faringitis akut'),
(5, 'Gondong'),
(6, 'ISPA'),
(7, 'Konjungtivitis'),
(8, 'Kusta I/T (MB)'),
(9, 'Tuberkulosa Paru BTA (+)'),
(10, 'Tuberkulosis paru klinis');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_penyakit`
--

CREATE TABLE `data_penyakit` (
  `id_data` int(11) NOT NULL,
  `penyakit` varchar(50) NOT NULL,
  `jan` int(11) NOT NULL,
  `feb` int(11) NOT NULL,
  `mar` int(11) NOT NULL,
  `apr` int(11) NOT NULL,
  `mei` int(11) NOT NULL,
  `jun` int(11) NOT NULL,
  `jul` int(11) NOT NULL,
  `ags` int(11) NOT NULL,
  `sep` int(11) NOT NULL,
  `okt` int(11) NOT NULL,
  `nov` int(11) NOT NULL,
  `des` int(11) NOT NULL,
  `tahun` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `data_penyakit`
--

INSERT INTO `data_penyakit` (`id_data`, `penyakit`, `jan`, `feb`, `mar`, `apr`, `mei`, `jun`, `jul`, `ags`, `sep`, `okt`, `nov`, `des`, `tahun`) VALUES
(1, 'Cacar air', 6, 6, 11, 10, 7, 6, 3, 3, 1, 7, 8, 5, 2015),
(2, 'Demam paratifoid', 4, 1, 6, 7, 6, 3, 3, 3, 4, 4, 1, 1, 2015),
(3, 'Diare dan Gastroenteritis', 42, 43, 29, 45, 23, 39, 20, 44, 42, 42, 55, 46, 2015),
(4, 'Faringitis akut', 35, 33, 24, 52, 32, 43, 27, 22, 29, 29, 32, 35, 2015),
(5, 'Gondong', 10, 7, 13, 8, 3, 13, 5, 4, 6, 1, 8, 5, 2015),
(6, 'ISPA', 448, 535, 578, 431, 294, 393, 258, 425, 508, 339, 458, 364, 2015),
(7, 'Konjungtivitis', 6, 8, 13, 16, 21, 89, 34, 14, 14, 12, 15, 13, 2015),
(8, 'Kusta I/T (MB)', 4, 8, 5, 9, 11, 12, 11, 11, 11, 13, 13, 14, 2015),
(9, 'Tuberkulosa Paru BTA (+)', 17, 18, 19, 34, 25, 22, 17, 16, 15, 48, 2, 4, 2015),
(10, 'Tuberkulosis paru klinis', 11, 12, 13, 9, 7, 8, 7, 10, 12, 2, 9, 6, 2015),
(11, 'Cacar air', 4, 3, 2, 3, 3, 1, 2, 5, 3, 6, 5, 4, 2016),
(12, 'Demam paratifoid', 2, 1, 3, 4, 3, 3, 3, 2, 2, 1, 1, 2, 2016),
(13, 'Diare dan Gastroenteritis', 42, 42, 15, 15, 21, 15, 30, 23, 20, 17, 18, 17, 2016),
(14, 'Faringitis akut', 11, 19, 36, 38, 25, 16, 55, 23, 29, 20, 23, 30, 2016),
(15, 'Gondong', 1, 4, 3, 4, 5, 5, 1, 1, 2, 1, 3, 1, 2016),
(16, 'ISPA', 338, 508, 511, 397, 281, 306, 247, 198, 582, 392, 473, 420, 2016),
(17, 'Konjungtivitis', 11, 6, 12, 8, 6, 6, 7, 4, 3, 3, 5, 7, 2016),
(18, 'Kusta I/T (MB)', 6, 2, 1, 2, 2, 3, 9, 8, 8, 9, 11, 6, 2016),
(19, 'Tuberkulosa Paru BTA (+)', 38, 34, 26, 25, 29, 22, 17, 17, 10, 27, 4, 2, 2016),
(20, 'Tuberkulosis paru klinis', 19, 20, 29, 22, 15, 11, 22, 18, 20, 7, 10, 3, 2016),
(21, 'Cacar air', 9, 7, 4, 6, 9, 4, 6, 2, 5, 20, 3, 3, 2017),
(22, 'Demam paratifoid', 5, 4, 6, 1, 2, 1, 3, 2, 1, 1, 2, 4, 2017),
(23, 'Diare dan Gastroenteritis', 28, 16, 31, 17, 22, 20, 21, 69, 42, 35, 37, 21, 2017),
(24, 'Faringitis akut', 29, 21, 46, 24, 19, 17, 27, 30, 35, 10, 5, 13, 2017),
(25, 'Gondong', 7, 5, 4, 1, 6, 5, 2, 4, 7, 1, 5, 2, 2017),
(26, 'ISPA', 411, 395, 480, 338, 273, 236, 289, 760, 571, 370, 452, 594, 2017),
(27, 'Konjungtivitis', 13, 9, 27, 11, 13, 16, 9, 12, 8, 9, 11, 20, 2017),
(28, 'Kusta I/T (MB)', 6, 1, 1, 2, 2, 3, 1, 3, 5, 5, 3, 7, 2017),
(29, 'Tuberkulosa Paru BTA (+)', 18, 26, 4, 6, 5, 3, 4, 5, 4, 8, 5, 1, 2017),
(30, 'Tuberkulosis paru klinis', 22, 28, 4, 4, 5, 1, 1, 6, 10, 2, 7, 2, 2017);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`id_akun`) USING BTREE;

--
-- Indexes for table `daftar_penyakit`
--
ALTER TABLE `daftar_penyakit`
  ADD PRIMARY KEY (`iddaftar_penyakit`) USING BTREE;

--
-- Indexes for table `data_penyakit`
--
ALTER TABLE `data_penyakit`
  ADD PRIMARY KEY (`id_data`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akun`
--
ALTER TABLE `akun`
  MODIFY `id_akun` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `daftar_penyakit`
--
ALTER TABLE `daftar_penyakit`
  MODIFY `iddaftar_penyakit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `data_penyakit`
--
ALTER TABLE `data_penyakit`
  MODIFY `id_data` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
