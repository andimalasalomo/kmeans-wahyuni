<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_global extends CI_Model {

    function get($table, $where,$select){
        if ($where == null) {
            $this->db->select($select);
			$result = $this->db->get($table)->result();
		}else{
            $this->db->select($select);
			$this->db->where($where);
			$result =  $this->db->get($table)->result();
        }
        return $result;
    }
    
    function insert($table, $data){
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }

    function insertBatch($table, $data){
        $this->db->insert_batch($table, $data);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }

    function update($table, $data, $where){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($table);
        $status = $this->db->affected_rows();
        return $status;
    }
    function updateBatch($table, $data, $where){
        $this->db->update_batch($table, $data, $where);
        $status = $this->db->affected_rows();
        return $status;
    }

    public function delete($table,$where){
		$this->db->where($where);
        $this->db->delete($table);
        $status = $this->db->affected_rows();
        return $status;
    }

    function count($count,$table,$where){
        $this->db->select($count);
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    function countAll($count,$table){
        $this->db->select($count);
        $this->db->from($table);
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }



}

/* End of file M_global.php */
