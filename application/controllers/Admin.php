<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('M_global');
        $halaman = $this->uri->segment(2);
        if ( !in_array($halaman, array('login'), true ) ) {
            if($this->session->userdata('LOGIN')!=TRUE){
				$this->session->set_flashdata('type','alert-warning');
                $this->session->set_flashdata('notif','Harap login terlebih dahulu.');
                redirect(base_url("login"));
			}
		}
	}

    public function index(){
        redirect(base_url("admin/dashboard"));  
    }
    public function dashboard(){
        $data['total_data'] = $this->M_global->countAll('COUNT(*) as jml','data_penyakit');
        $data['total_penyakit'] = $this->M_global->countAll('count(DISTINCT(penyakit)) as jml','data_penyakit');

        $this->load->view('admin/skeleton/skeleton_header');
        $this->load->view('admin/skeleton/skeleton_navbar');
        $this->load->view('admin/pages/dashboard',$data);        
        $this->load->view('admin/skeleton/skeleton_footer');
    }
    public function data(){

        if($this->input->post('deleteData')){
			$tahun = $this->input->post('tahun');
			$where = array(
				'tahun' => $tahun
			);
			$delete = $this->M_global->delete('data_penyakit',$where);
			echo "<script>alert('Data berhasil dihapus.')</script>";
			echo "<script> window.location.href = ''</script>";
        }
        

        $data['data_penyakit'] = $this->M_global->get('data_penyakit','','DISTINCT(tahun)');
        $this->load->view('admin/skeleton/skeleton_header');
        $this->load->view('admin/skeleton/skeleton_navbar');
        $this->load->view('admin/pages/manage_data',$data);        
        $this->load->view('admin/skeleton/skeleton_footer');
    }
    public function add(){
        
        $data['daftar_penyakit'] = $this->M_global->get('daftar_penyakit','','*');

        $this->load->view('admin/skeleton/skeleton_header');
        $this->load->view('admin/skeleton/skeleton_navbar');
        $this->load->view('admin/pages/add_data',$data);        
        $this->load->view('admin/skeleton/skeleton_footer');
    }
    public function doinsertdata(){
        $index = 1;
        $post = $this->input->post();
        $total = sizeof($post['penyakit']);
        $data = array();
        for ($i=1; $i <= $total; $i++) { 
          $dump = array(  
            'penyakit' => $post['penyakit'][$i],
            'jan' => $post['jan'][$i],
            'feb' => $post['feb'][$i],
            'mar' => $post['mar'][$i],
            'apr' => $post['apr'][$i],
            'mei' => $post['mei'][$i],
            'jun' => $post['jun'][$i],
            'jul' => $post['jul'][$i],
            'ags' => $post['ags'][$i],
            'sep' => $post['sep'][$i],
            'okt' => $post['okt'][$i],
            'nov' => $post['nov'][$i],
            'des' => $post['des'][$i],
            'tahun' => $post['tahun'],
           );
           array_push($data,$dump);
        }
        $save = $this->M_global->insertBatch('data_penyakit',$data);
        if ($save > 0) {
            $this->session->set_flashdata('type','alert-success');
			$this->session->set_flashdata('notif','Data berhasil disimpan.');
        }else{
            $this->session->set_flashdata('type','alert-danger');
			$this->session->set_flashdata('notif','Data gagal disimpan, Silakan ulangi !!!');
        }
        redirect('admin/add');
        
    }
    public function edit(){
        $where = array(
            'tahun' => $this->input->post('tahun')
        );
        $data['data_edit'] = $this->M_global->get('data_penyakit',$where,'*');
        $data['tahun'] = $this->input->post('tahun');

        $this->load->view('admin/skeleton/skeleton_header');
        $this->load->view('admin/skeleton/skeleton_navbar');
        $this->load->view('admin/pages/edit_data',$data);        
        $this->load->view('admin/skeleton/skeleton_footer');
    }

    public function doeditdata(){
        $post = $this->input->post();
        $total = sizeof($post['penyakit']);
        $data = array();
        for ($i=1; $i <= $total; $i++) { 
          $dump = array(
            'id_data' => $post['id_data'][$i],  
            'penyakit' => $post['penyakit'][$i],
            'jan' => $post['jan'][$i],
            'feb' => $post['feb'][$i],
            'mar' => $post['mar'][$i],
            'apr' => $post['apr'][$i],
            'mei' => $post['mei'][$i],
            'jun' => $post['jun'][$i],
            'jul' => $post['jul'][$i],
            'ags' => $post['ags'][$i],
            'sep' => $post['sep'][$i],
            'okt' => $post['okt'][$i],
            'nov' => $post['nov'][$i],
            'des' => $post['des'][$i],
            'tahun' => $post['tahun'],
           );
           array_push($data,$dump);
        }
        $save = $this->M_global->updateBatch('data_penyakit',$data,'id_data');
        if ($save > 0) {
            $this->session->set_flashdata('type','alert-success');
			$this->session->set_flashdata('notif','Data berhasil diubah.');
        }else{
            $this->session->set_flashdata('type','alert-warning');
			$this->session->set_flashdata('notif','Data gagal diubah atau tidak terjadi perubahan');
        }
        redirect('admin/data');
        
    }

    public function view(){
        $where = array(
            'tahun' => $this->input->post('tahun')
        );
        $data['data_edit'] = $this->M_global->get('data_penyakit',$where,'*');
        $data['tahun'] = $this->input->post('tahun');

        $this->load->view('admin/skeleton/skeleton_header');
        $this->load->view('admin/skeleton/skeleton_navbar');
        $this->load->view('admin/pages/view_data',$data);        
        $this->load->view('admin/skeleton/skeleton_footer');
    }

    public function penyakit(){
        $data['data_penyakit'] = $this->M_global->get('daftar_penyakit','','*');
        $submit = $this->input->post('submit');
        if (isset($submit)) {
            $penyakit = array(
                'nama_penyakit' => $this->input->post('penyakit')
            );
            $save = $this->M_global->insert('daftar_penyakit',$penyakit);
            if ($save) {
                $this->session->set_flashdata('type','alert-success');
                $this->session->set_flashdata('notif','Data berhasil disimpan.');
            }else{
                $this->session->set_flashdata('type','alert-danger');
                $this->session->set_flashdata('notif','Data gagal disimpan, Silakan ulangi !!!');
            }
            redirect(base_url('admin/penyakit'),'refresh');
        }
        $this->load->view('admin/skeleton/skeleton_header');
        $this->load->view('admin/skeleton/skeleton_navbar');
        $this->load->view('admin/pages/penyakit',$data);        
        $this->load->view('admin/skeleton/skeleton_footer');
    }


}

/* End of file Admin.php */
