<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('M_global');
	}

    public function index(){
        $this->load->view('user/skeleton/skeleton_header');
        $this->load->view('user/skeleton/skeleton_navbar');
        $this->load->view('user/pages/home');
        $this->load->view('user/skeleton/skeleton_footer');
        
    }
    
    public function data(){
        $data['viewdata'] ="";
        $data['data_penyakit'] = $this->M_global->get('data_penyakit','','DISTINCT(tahun)');
        $this->load->view('user/skeleton/skeleton_header');
        $this->load->view('user/skeleton/skeleton_navbar');
        $this->load->view('user/pages/data',$data);        
        $this->load->view('user/skeleton/skeleton_footer');
    }
    
    public function searchdata(){
        $where = array(
            'tahun' => $this->input->post('thn1')
        );
        
        $data['data_edit'] = $this->M_global->get('data_penyakit',$where,'*');
        $data['tahun1'] = $this->input->post('thn1');
        $data['viewdata'] ="isi";

        $data['data_penyakit'] = $this->M_global->get('data_penyakit','','DISTINCT(tahun)');
        
        $this->load->view('user/skeleton/skeleton_header');
        $this->load->view('user/skeleton/skeleton_navbar');
        $this->load->view('user/pages/data',$data);        
        $this->load->view('user/skeleton/skeleton_footer');
    }

    public function perhitungan(){
        $data['tahun'] = $this->M_global->get('data_penyakit','','DISTINCT(tahun)');
        $data['view_data'] = false;

        $submit = $this->input->post('submit');
        if (isset($submit)) {
            $tahun = $this->input->post('tahun');
            $data['view_data'] = true;
            $data['selected_tahun'] = $tahun;
            $data['perhitungan'] = $this->perhitungan_raw($tahun);
        }
        
        $this->load->view('user/skeleton/skeleton_header');
        $this->load->view('user/skeleton/skeleton_navbar');
        $this->load->view('user/pages/perhitungan',$data);        
        $this->load->view('user/skeleton/skeleton_footer');
    }

    public function tentang(){
        $this->load->view('user/skeleton/skeleton_header');
        $this->load->view('user/skeleton/skeleton_navbar');
        $this->load->view('user/pages/tentang');        
        $this->load->view('user/skeleton/skeleton_footer');
    }

    public function grafik(){
        $data['penyakit'] = $this->M_global->get('daftar_penyakit','','*');
        foreach ($data['penyakit'] as $key ) {
            $no = $key->iddaftar_penyakit;
            $where = array(
                'penyakit' => $key->nama_penyakit,
            );
            $data['data_penyakit_'.$no] = $this->M_global->get('data_penyakit',$where,'*');
        }
        $this->load->view('user/skeleton/skeleton_header');
        $this->load->view('user/skeleton/skeleton_navbar');
        $this->load->view('user/pages/grafik', $data);        
        $this->load->view('user/skeleton/skeleton_footer');
    }

    public function perhitungan_raw($tahun){
        function centroid($c1, $c2, $c3, $data_bulan, $data_penyakit){
            $c[1]  = $c1;
            $c[2]  = $c2;
            $c[3]  = $c3;
            for($yy=1;$yy<=3;$yy++){
                $hasil[$yy] = array();
                foreach ($data_bulan as $key) {
                    $arrayData = array();
                    foreach ($key as $value) {
                        array_push($arrayData, $value);
                    }
                    $arrayCent = array();
                    foreach ($c[$yy] as $cval) {
                        array_push($arrayCent, $cval);
                    }
                    $x = 0;
                    for($i=2;$i < (sizeof($arrayData) - 1);$i++){
                        $y[$i] = $arrayData[$i] - $arrayCent[$i];
                        $x += pow($y[$i],2);
                    }
                    $result = sqrt($x);
                    array_push($hasil[$yy], $result);
                    
                }
            }

            $no = 0;
            foreach($data_penyakit as $val){
                $penyakit = $val->penyakit;
                $min = array(
                    "c1" => $hasil[1][$no],
                    "c2" => $hasil[2][$no],
                    "c3" => $hasil[3][$no]
                );

                $min_h = array_keys($min, min($min));

                $new[$penyakit] = array(
                    "jan" => $val->jan,
                    "feb" => $val->feb,
                    "mar" => $val->mar,
                    "apr" => $val->apr,
                    "mei" => $val->mei,
                    "jun" => $val->jun,
                    "jul" => $val->jul,
                    "ags" => $val->ags,
                    "sep" => $val->sep,
                    "okt" => $val->okt,
                    "nov" => $val->nov,
                    "des" => $val->des,
                    "c1" => $hasil[1][$no],
                    "c2" => $hasil[2][$no],
                    "c3" => $hasil[3][$no],
                    "min" => $min_h[0]);
                $no++;
            }

            $no = 0;
            foreach($data_penyakit as $val){
                $penyakit = $val->penyakit;
                $min = array(
                    "c1" => $hasil[1][$no],
                    "c2" => $hasil[2][$no],
                    "c3" => $hasil[3][$no]
                );

                $min_h = array_keys($min, min($min));

                $t[$penyakit] = array(
                    "min" => $min_h[0]);
                $no++;
            }

            $c1=0;
            $c2=0;
            $c3=0;
            foreach($new as $val){
                $cluseter[] = $val['min'];
            }
            $counts = array_count_values($cluseter);

            $output = array(
                "hasil" => $new,
                "t" => $t,
                "c1" => $counts['c1'],
                "c2" => $counts['c2'],
                "c3" => $counts['c3']
            );
            return $output;
        }

        function iterasi($data){
            for($i=1; $i<=3; $i++){
                $jan = 0;
                $feb = 0;
                $mar = 0;
                $apr = 0;
                $mei = 0;
                $jun = 0;
                $jul = 0;
                $ags = 0;
                $sep = 0;
                $okt = 0;
                $nov = 0;
                $des = 0;
                foreach($data['hasil'] as $obj){
                    if($obj['min'] == 'c'.$i){
                        $jan = $jan + $obj['jan'];
                        $feb = $feb + $obj['feb'];
                        $mar = $mar + $obj['mar'];
                        $apr = $apr + $obj['apr'];
                        $mei = $mei + $obj['mei'];
                        $jun = $jun + $obj['jun'];
                        $jul = $jul + $obj['jul'];
                        $ags = $ags + $obj['ags'];
                        $sep = $sep + $obj['sep'];
                        $okt = $okt + $obj['okt'];
                        $nov = $nov + $obj['nov'];
                        $des = $des + $obj['des'];
                    }
                }
                $c[$i] = (object) array(
                    "id_data" => '',
                    "penyakit" => '',
                    "jan" => $jan / $data['c'.$i],
                    "feb" => $feb / $data['c'.$i],
                    "mar" => $mar / $data['c'.$i],
                    "apr" => $apr / $data['c'.$i],
                    "mei" => $mei / $data['c'.$i],
                    "jun" => $jun / $data['c'.$i],
                    "jul" => $jul / $data['c'.$i],
                    "ags" => $ags / $data['c'.$i],
                    "sep" => $sep / $data['c'.$i],
                    "okt" => $okt / $data['c'.$i],
                    "nov" => $nov / $data['c'.$i],
                    "des" => $des / $data['c'.$i],
                );
            }
            $hasil = array(
                "c1" => $c[1],
                "c2" => $c[2],
                "c3" => $c[3]
            );
            return $hasil;
        }

        $where = array(
            'tahun' => $tahun
        );
        $data_bulan = $this->M_global->get('data_penyakit',$where,'*');
        $data_penyakit = $this->M_global->get('data_penyakit',$where,'*');
        
        //Iterasi pertama
        $c1 = $data_bulan['5'];
        $c2 = $data_bulan['2'];
        $c3 = $data_bulan['4'];
        $centroid[1] = centroid($c1, $c2, $c3, $data_bulan, $data_penyakit);
        $iterasi[1] = iterasi($centroid[1]);
        $data['interasi_1'] = $centroid[1];
        //print_r($centroid[1]);
        //print_r($iterasi[1]);
        $data['centroid_1'] = array(
            'c1' => $c1,
            'c2' => $c2,
            'c3' => $c3,
        );

        //Iterasi kedua
        $c1  = $iterasi[1]['c1'];
        $c2  = $iterasi[1]['c2'];
        $c3  = $iterasi[1]['c3'];
        $centroid[2] = centroid($c1, $c2, $c3, $data_bulan, $data_penyakit);
        $iterasi[2] = iterasi($centroid[2]);
        $data['interasi_2'] = $centroid[2];
        $data['centroid_2'] = array(
            'c1' => $c1,
            'c2' => $c2,
            'c3' => $c3,
        );
        //print_r($centroid[2]);
        //print_r($iterasi[2]);
        
        if($centroid[1]['t'] == $centroid[2]['t']){
            $gab = $centroid[2]['hasil'];
            foreach ($gab as $key => $row) {
                $cluster[$key]  = $row['min'];
            }
            $cluster  = array_column($gab, 'min');
            array_multisort($cluster, SORT_ASC, $gab);
            $data['gabungan'] = $gab;
            return $data;
        }else{
            //Iterasi ke-x
            for($i=2;$i<=100;$i++){
                $r = $i + 1;
                $c1 = $iterasi[$i]['c1'];
                $c2 = $iterasi[$i]['c2'];
                $c3 = $iterasi[$i]['c3'];
                $centroid[$r] = centroid($c1, $c2, $c3, $data_bulan, $data_penyakit);
                $iterasi[$r] = iterasi($centroid[$r]);
                    print_r($centroid[$r]);
                    //print_r($iterasi[$r]);
                    $data['interasi_'.$r] = $centroid[$r];
                    $data['centroid_'.$r] = array(
                        'c1' => $c1,
                        'c2' => $c2,
                        'c3' => $c3,
                    );
                if($centroid[$i]['t'] == $centroid[$r]['t']){
                    $gab = $centroid[$r]['hasil'];
                    foreach ($gab as $key => $row) {
                        $cluster[$key]  = $row['min'];
                    }
                    $cluster  = array_column($gab, 'min');
                    array_multisort($cluster, SORT_ASC, $gab);
                    $data['gabungan'] = $gab;
                    return $data;
                    break;
                }else{
                }
            }
        }

    }

}

/* End of file User.php */
