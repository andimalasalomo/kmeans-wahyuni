<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">
<?php $curr_url = uri_string(); ?>
  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<?php echo base_url("user/index");?>" class="navbar-brand"><b>PUSKESMAS JATIBENING</b></a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="<?php if($curr_url == "user/index"){ echo "active";}?>"><a href="<?php echo base_url("user/index");?>">Home <span class="sr-only">(current)</span></a></li>
            <li class="<?php if($curr_url == "user/data" or $curr_url == "user/searchdata"){ echo "active";}?>"><a href="<?php echo base_url("user/data");?>">Data</a></li>
            <li class="<?php if($curr_url == "user/grafik"){ echo "active";}?>"><a href="<?php echo base_url("user/grafik");?>">Grafik</a></li>
            <li class="<?php if($curr_url == "user/perhitungan"){ echo "active";}?>"><a href="<?php echo base_url("user/perhitungan");?>">Perhitungan</a></li>
            <li class="<?php if($curr_url == "user/tentang"){ echo "active";}?>"><a href="<?php echo base_url("user/tentang");?>">Tentang</a></li>
          </ul>
        </div>
        <div class="collapse navbar-collapse pull-right" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="<?php echo base_url("login");?>"><i class="fa fa-sign-in"></i> Login</a></li>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>