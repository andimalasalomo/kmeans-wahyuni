 <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <br>
      </section>

      <!-- Main content -->
      <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Tentang Website</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
              <p style="text-align: justify;"><img class="img-responsive" src="<?php echo base_url('assets/dist/img/tentang.jpg'); ?>" alt="Home" style="float: left; width: 400px; margin-right: 10px;">Website ini menampilkan data penyakit yang paling sering diderita oleh pasien di Puskesmas Jatibening. Data tersebut akan dikelompokan menggunakan algoritma K-Means dengan metode Clustering. Data diperoleh dari Puskesmas Jatibening dengan dimulai dari tahun 2015. Website ini dapat digunakan untuk melakukan perbandingan tingkat penderita penyakit di Puskesmas Jatibening.
              </p><br><br>
              <p style="text-align: justify;">
                <b>Kontak :</b><br>
                Alamat	: Jalan Amarilis, RT. 2 / RW. 12, Pondok Gede, Jatibening, Kota Bekasi, Jawa Barat 17412 <br>
                Telepon	: (021) 84994628
              </p><hr>
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.0145450541413!2d106.95397191476935!3d-6.26181379546761!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e698d06526b9e4d%3A0x3676655efbc38e5!2sUPTD+Puskesmas+Jatibening!5e0!3m2!1sid!2sid!4v1533311745050" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->