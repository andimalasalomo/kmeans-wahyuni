 <!-- ChartJS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
 <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <br>
      </section>

      <!-- Main content -->
      <section class="content">
     
      </script>
      <?php foreach($penyakit as $data) { $id = $data->iddaftar_penyakit; ?>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $data->nama_penyakit; ?></h3>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="canvas_<?php echo $id; ?>" style="height:250px"></canvas>
                <script>
                  var speedCanvas = document.getElementById("canvas_<?php echo $id; ?>");
                  var speedData = {
                    labels: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Ags", "Sep", "Okt", "Nov", "Des"],
                    datasets: [
                    <?php
                      foreach (${'data_penyakit_'.$id} as $key) {
                      $randomcolor = '#' . strtoupper(dechex(rand(0,10000000)));
                    ?>
                      {
                        label: "<?php echo $key->tahun; ?>",
                        data: [<?php echo $key->jan.','.$key->feb.','.$key->mar.','.$key->apr.','.$key->mei.','.$key->jun.','.$key->jul.','.$key->ags.','.$key->sep.','.$key->okt.','.$key->nov.','.$key->des.','; ?>],
                        borderColor: '<?php echo $randomcolor; ?>',
                        backgroundColor: 'transparent',
                        pointBorderColor: '<?php echo $randomcolor; ?>',
                        pointBackgroundColor: '<?php echo $randomcolor; ?>',
                        pointBorderWidth: 2,
                      },
                    <?php    
                      } 
                    ?>  
                    ]
                  };

                  var chartOptions = {
                    legend: {
                      display: true,
                      position: 'top',
                      labels: {
                          boxWidth: 80,
                          fontColor: 'black'
                        }
                      },
                      tooltips: {
                        mode: 'index',
                        position: 'nearest',
                      },
                  };

                  var lineChart = new Chart(speedCanvas, {
                    type: 'line',
                    data: speedData,
                    options: chartOptions
                  });
                  </script>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
      <?php } ?>
      <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
