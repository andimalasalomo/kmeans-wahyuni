 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Perhitungan
        <small>Perhitungan data puskesmas</small>
      </h1>
    </section><br>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- Default box -->
        <div class="col-md-12 col-xs-12">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Pilih Tahun</h3>
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <form action="<?php echo base_url('user/perhitungan'); ?>" method="post">
                  <select required name="tahun" class="form-control">
                    <option selected disabled value="">Silahkan Pilih Tahun</option>
                    <?php
                    foreach ($tahun as $data) {
                        $tahun = $data->tahun;
                    ?>
                      <option value="<?php echo $tahun; ?>"><?php echo $tahun; ?></option>
                    <?php
                    }
                    ?>
                  </select>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                  <button type="submit" name="submit" value="submit"  class="btn btn-primary">Proses</button>
                </form>
              </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.box -->
      </div>
      <?php if($view_data == false){ }else{ ?>
      <div class="row">
        <!-- Default box -->
        <div class="col-md-12 col-xs-12">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Hasil Perhitungan Tahun <?php echo $selected_tahun; ?></h3>
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <?php
                  $j = (sizeof($perhitungan) - 1)/2;
                  $total = array();
                  foreach ($perhitungan['centroid_'.$j] as $key => $value) {
                    $sum = array( $key => array_sum((array) $value));
                    $total += $sum;
                  }
                  arsort($total);
                  $label = array('Tinggi','Sedang','Rendah');
                  $new_label = array();
                  $no = 0;
                  foreach ($total as $key => $value) {
                      $old = array( $key => $label[$no]);
                      $new_label += $old;
                      $no++;
                  }
                ?>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Jenis Penyakit</th>
                        <th>Jan</th>
                        <th>Feb</th>
                        <th>Mar</th>
                        <th>Apr</th>
                        <th>Mei</th>
                        <th>Jun</th>
                        <th>Jul</th>
                        <th>ags</th>
                        <th>Sep</th>
                        <th>Okt</th>
                        <th>Nov</th>
                        <th>Des</th>
                        <th>Cluster</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php 
                    //print_r($perhitungan);
                    foreach ($perhitungan['gabungan'] as $key => $value) {
                        $diagnosa = $key;
                        
                        $jan = $value['jan'];
                        $feb = $value['feb'];
                        $mar = $value['mar'];
                        $apr = $value['apr'];
                        $mei = $value['mei'];
                        $jun = $value['jun'];
                        $jul = $value['jul'];
                        $ags = $value['ags'];
                        $sep = $value['sep'];
                        $okt = $value['okt'];
                        $nov = $value['nov'];
                        $des = $value['des'];
                        $min = $value['min'];
                        if ($min == "c1") {
                        $class  = "danger";
                        }elseif ($min == "c2") {
                        $class = "warning";
                        }elseif ($min = "c3") {
                        $class = "info";
                        }
                    ?>
                        <tr class="<?php echo $class; ?>">
                        <td><?php echo $diagnosa; ?></td>
                        <td><?php echo $jan; ?></td>
                        <td><?php echo $feb; ?></td>
                        <td><?php echo $mar; ?></td>
                        <td><?php echo $apr; ?></td>
                        <td><?php echo $mei; ?></td>
                        <td><?php echo $jun; ?></td>
                        <td><?php echo $jul; ?></td>
                        <td><?php echo $ags; ?></td>
                        <td><?php echo $sep; ?></td>
                        <td><?php echo $okt; ?></td>
                        <td><?php echo $nov; ?></td>
                        <td><?php echo $des; ?></td>
                        <td><?php echo strtoupper($min).' ('.$new_label[$min].')'; ?></td>
                        </tr>
                    <?php
                    }
                    ?>
                    </tbody>
                </table>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                  <button type="submit" onclick="display_detail();" class="btn btn-primary">Lihat Detail Perhitungan</button>
              </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.box -->
      </div>
      <?php } ?>
      <div  id="detail" class="row" style="display: none">
        <!-- Default box -->
        <div class="col-md-12 col-xs-12">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Detail Perhitungan</h3>
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <?php
                //print_r($perhitungan);
                for ($i=1; $i <= $j ; $i++) {
                ?>
                <h3><?php echo "Iterasi $i"; ?></h3><br>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                        <th>Centriod <?php echo $i; ?></th>
                        <th>B1</th>
                        <th>B2</th>
                        <th>B3</th>
                        <th>B4</th>
                        <th>B5</th>
                        <th>B6</th>
                        <th>B7</th>
                        <th>B8</th>
                        <th>B9</th>
                        <th>B10</th>
                        <th>B11</th>
                        <th>B12</th>
                        </tr>
                    </thead>
                    <tbody>
                <?php
                $centroid = $perhitungan['centroid_'.$i];
                foreach ($centroid as $key => $value) {
                    $cluster = $key;
                    $jan = $value->jan;
                    $feb = $value->feb;
                    $mar = $value->mar;
                    $apr = $value->apr;
                    $mei = $value->mei;
                    $jun = $value->jun;
                    $jul = $value->jul;
                    $ags = $value->ags;
                    $sep = $value->sep;
                    $okt = $value->okt;
                    $nov = $value->nov;
                    $des = $value->des;
                    if ($cluster == "c1") {
                    $class  = "danger";
                    }elseif ($cluster == "c2") {
                    $class = "warning";
                    }elseif ($cluster = "c3") {
                    $class = "info";
                    }
                    ?>
                    <tr class="<?php echo $class; ?>">
                    <td><?php echo $cluster; ?></td>
                    <td><?php echo $jan; ?></td>
                    <td><?php echo $feb; ?></td>
                    <td><?php echo $mar; ?></td>
                    <td><?php echo $apr; ?></td>
                    <td><?php echo $mei; ?></td>
                    <td><?php echo $jun; ?></td>
                    <td><?php echo $jul; ?></td>
                    <td><?php echo $ags; ?></td>
                    <td><?php echo $sep; ?></td>
                    <td><?php echo $okt; ?></td>
                    <td><?php echo $nov; ?></td>
                    <td><?php echo $des; ?></td>
                    </tr>
                <?php    
                }
                ?>
                    </tbody>
                    </table></div><br>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                        <th>Jenis Penyakit</th>
                        <th>Jan</th>
                        <th>Feb</th>
                        <th>Mar</th>
                        <th>Apr</th>
                        <th>Mei</th>
                        <th>Jun</th>
                        <th>Jul</th>
                        <th>ags</th>
                        <th>Sep</th>
                        <th>Okt</th>
                        <th>Nov</th>
                        <th>Des</th>
                        <th>C1</th>
                        <th>C2</th>
                        <th>C3</th>
                        <th>Cluster</th>
                        </tr>
                    </thead>
                    <tbody>
                <?php   
                $hasil = $perhitungan['interasi_'.$i]['hasil'];
                foreach ($hasil as $key => $value) {
                        $diagnosa = $key;
                        $jan = $value['jan'];
                        $feb = $value['feb'];
                        $mar = $value['mar'];
                        $apr = $value['apr'];
                        $mei = $value['mei'];
                        $jun = $value['jun'];
                        $jul = $value['jul'];
                        $ags = $value['ags'];
                        $sep = $value['sep'];
                        $okt = $value['okt'];
                        $nov = $value['nov'];
                        $des = $value['des'];
                        $c1 = $value['c1'];
                        $c2 = $value['c2'];
                        $c3 = $value['c3'];
                        $min = $value['min'];
                        if ($min == "c1") {
                        $class  = "danger";
                        }elseif ($min == "c2") {
                        $class = "warning";
                        }elseif ($min = "c3") {
                        $class = "info";
                        }
                ?>
                    <tr class="<?php echo $class; ?>">
                    <td><?php echo $diagnosa; ?></td>
                    <td><?php echo $jan; ?></td>
                    <td><?php echo $feb; ?></td>
                    <td><?php echo $mar; ?></td>
                    <td><?php echo $apr; ?></td>
                    <td><?php echo $mei; ?></td>
                    <td><?php echo $jun; ?></td>
                    <td><?php echo $jul; ?></td>
                    <td><?php echo $ags; ?></td>
                    <td><?php echo $sep; ?></td>
                    <td><?php echo $okt; ?></td>
                    <td><?php echo $nov; ?></td>
                    <td><?php echo $des; ?></td>
                    <td><?php echo $c1; ?></td>
                    <td><?php echo $c2; ?></td>
                    <td><?php echo $c3; ?></td>
                    <td><?php echo $min; ?></td>
                    </tr>
                <?php        
                    }
                ?>
                    </tbody>
                    </table><br><br><hr>
                <?php
                }
                ?>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.box -->
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
