 <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <br>
      </section>

      <!-- Main content -->
      <section class="content">
      <div class="row">
        <center>
          <img class="img-responsive" src="<?php echo base_url('assets/dist/img/login.png'); ?>" alt="Home" style="align: center; width: 250px; margin-top: -40px;">
        </center>
        <div class="col-xs-12">
          <div class="box box-primary">
          <div class="box-header">
              <h3 class="box-title">SELAMAT DATANG DI PUSKEMAS JATIBENING</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
              <p style="text-align: justify;"><img class="img-responsive" src="<?php echo base_url('assets/dist/img/home.jpg'); ?>" alt="Home" style="float: left; width: 400px; margin-right: 10px;"> <br><br> Penyakit tropis merupakan penyakit yang banyak ditemukan di daerah tropis dan subtropis yang umumnya berupa infeksi. Penyakit Tropis tersebut terbagi atas 4 macam yaitu infeksi yang disebabkan oleh bakteri seperti demam tifoid, infeksi yang disebabkan oleh virus seperti DBD, infeksi yang disebabkan oleh parasit seperti Malaria, dan sindrom penyakit menular seperti ISPA. <br><br>
              Beberapa contoh penyakit yang disebabkan oleh virus, bakteri, dan virus/bakteri. <br>
              Peyakit yang disebabkan oleh virus : Cacar air, Gondong, dan ISPA <br>
              Penyakit yang disebabkan oleh bakteri : Demam Paratifoid, Diare, Kusta, dan Tuberkulosis <br>
              Penyakit yang disebabkan oleh virus/bakteri : Faringitis dan Konjungtivitis <br>    
              </p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->