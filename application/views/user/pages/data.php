 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Lihat Data
        <small>Lihat data penyakit</small>
      </h1>
    </section><br>
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Data Penyakit</h3>
        </div>
        <div class="box-body">
            <form class="" action="<?php echo base_url("user/searchdata"); ?>" method="post">
              <div class="box-body">
                  <div class="row">   
                    <div class="col-md-12">
                      <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-1 control-label">Tahun</label>
                        </div>
                      </div>
                    </div>   
                  </div>
                    <div class="col-md-5">
                      <div class="box-body">
                      <select class="form-control" name="thn1" required>
                        <option value='' selected disabled>Silahkan Pilih Tahun</option>
                           <?php
                            $no = 1;
                            foreach ($data_penyakit as $data) {
                                $tahun = $data->tahun;
                                ?>
                              <option value="<?php echo $tahun; ?>"><?php echo $tahun; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                </div>
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Lihat</button>
                </div>
            </form>
        </div>
      </div>

      <?php if($viewdata==""){ } 
      else{?>
      
       <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Data Penyakit Tahun <?php echo $tahun1 ?></h3>
        </div>
        <div class="box-body">
          <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th >No</th>
                  <th style="width: 350px">Penyakit</th>
                  <th >Jan</th>
                  <th >Feb</th>
                  <th >Mar</th>
                  <th >Apr</th>
                  <th >Mei</th>
                  <th >Jun</th>
                  <th >Jul</th>
                  <th >Ags</th>
                  <th >Sep</th>
                  <th >Okt</th>
                  <th >Nov</th>
                  <th >Des</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    $no = 1;
                    foreach ($data_edit as $data) {
                        $penyakit = $data->penyakit;
                        $jan = $data->jan;
                        $feb = $data->feb;
                        $mar = $data->mar;
                        $apr = $data->apr;
                        $mei = $data->mei;
                        $jun = $data->jun;
                        $jul = $data->jul;
                        $ags = $data->ags;
                        $sep = $data->sep;
                        $okt = $data->okt;
                        $nov = $data->nov;
                        $des = $data->des;
                        ?>
                        <tr>
                            <td><?php echo $no++ ?></td>
                            <td><?php echo $penyakit ?></td>
                            <td><?php echo $jan ?></td>
                            <td><?php echo $feb ?></td>
                            <td><?php echo $mar ?></td>
                            <td><?php echo $apr ?></td>
                            <td><?php echo $mei ?></td>
                            <td><?php echo $jun ?></td>
                            <td><?php echo $jul ?></td>
                            <td><?php echo $ags ?></td>
                            <td><?php echo $sep ?></td>
                            <td><?php echo $okt ?></td>
                            <td><?php echo $nov ?></td>
                            <td><?php echo $des ?></td>
                        </tr>
                        <?php
                    }
                ?>
                </tbody>
              </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <!-- Footer -->
        </div>
        <!-- /.box-footer-->
      </div>

      <?php } ?>

      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </div>
  </div>
  <!-- /.content-wrapper -->
