<!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <?php $curr_url = uri_string(); ?>
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">NAVIGASI</li>
        <li class="<?php if ($curr_url == "admin/dashboard") { echo " active"; } ?>">
          <a href="<?php echo base_url('admin/dashboard'); ?>">
          <i class="fa fa-dashboard"></i><span>Home</span></a>
        </li>
        <li class="treeview <?php if ($curr_url == "admin/penyakit" or $curr_url == "admin/data" or $curr_url == "admin/view" or $curr_url == "admin/add" or $curr_url == "admin/edit") { echo " active"; } ?>">
          <a href="#">
            <i class="fa fa-archive"></i> <span>Manajemen Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if ($curr_url == "admin/penyakit") { echo " active"; } ?>"><a href="<?php echo base_url('admin/penyakit'); ?>"><i class="fa fa-circle-o"></i> Data penyakit</a></li>
            <li class="<?php if ($curr_url == "admin/data" or $curr_url == "admin/edit" or $curr_url == "admin/view") { echo " active"; } ?>"><a href="<?php echo base_url('admin/data'); ?>"><i class="fa fa-circle-o"></i> Daftar Data Set</a></li>
            <li class="<?php if ($curr_url == "admin/add") { echo " active"; } ?>"><a href="<?php echo base_url('admin/add'); ?>"><i class="fa fa-circle-o"></i> Tambah Data Set</a></li>
          </ul>
        </li>
        </li>
      </ul>
    </section>
    
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->
