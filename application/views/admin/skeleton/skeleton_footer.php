  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong><b>Kontak  : 085691010756</b><br>
    Email   : wahyunitu@gmail.com<br>
    Tentang : Aplikasi Data Mining Clustering Penyakit Pada Puskesmas Jatibening
  </footer>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url("assets/bower_components/jquery/dist/jquery.min.js");?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url("assets/bower_components/bootstrap/dist/js/bootstrap.min.js");?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url("assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js");?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url("assets/bower_components/fastclick/lib/fastclick.js");?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url("assets/dist/js/adminlte.min.js");?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url("assets/dist/js/demo.js");?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js");?>"></script>
<script src="<?php echo base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"></script>
<script src="https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>

<script>
  //datatable
  $(function () {
    $('#example1').DataTable({
      'responsive': true,
  });
  });
  $('#lol').DataTable();
  $('#alert').delay(2500).fadeOut(); 
</script>
</body>
</html>
