 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manajemen Data
        <small>Manajemen data penyakit</small>
      </h1>
      <ol class="breadcrumb">
          <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Manajemen Data</li>
      </ol>
        <?php if($this->session->flashdata('notif')){
            $type = $this->session->flashdata('type');
            echo "<br><div id='alert' class='alert $type'>";
            echo $this->session->flashdata('notif').'</div>';} 
        ?>
    </section><br>
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Tambah Data Penyakit</h3>
        </div>
        <div class="box-body">
          <div class="col-md-12">
          <form action="<?php echo base_url('admin/penyakit') ?>" method="post">
            <div class="form-horizontal">
              <div class="form-group">
                  <label class="col-sm-2 control-label">Nama Penyakit</label>
                  <div class="col-sm-10">
                    <input required type="text" name="penyakit" class="form-control" placeholder="Masukan Penyakit" autofocus >
                  </div>
              </div>
            </div>
          </div> 
        </div>
        <!-- /.box-body -->
      <div class="box-footer">
        <button onclick="return confirm('Apakah data yg diisikan sudah benar? jika sudah benar, data yang disimpan tidak dapat diubah');" type="submit" name="submit" value="submit" class="btn btn-primary">Simpan</button>
        <button type="reset" class="btn btn-danger">Reset</button>
      </div>
        </form>
      </div>

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Daftar Penyakit</h3>
        </div>
        <div class="box-body">
          <div class="col-md-12">
          <table id="example1" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>No</th>
                <th>Penyakit</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $no = 1;
              foreach ($data_penyakit as $key ) {
                echo "
                  <tr>
                    <td>$no</td>
                    <td>$key->nama_penyakit</td>
                  </tr>
                ";
                $no++;
              }
              ?>
            </tbody>
          </table>
          </div> 
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
    </div>
  </div>
  <!-- /.content-wrapper -->
