 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manajemen Data
        <small>Manajemen data penyakit</small>
      </h1>
      <ol class="breadcrumb">
          <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="data">Manajemen Data</a></li>
          <li class="active">Lihat Data</li>
      </ol>
        <?php if($this->session->flashdata('notif')){
            $type = $this->session->flashdata('type');
            echo "<br><div id='alert' class='alert $type'>";
            echo $this->session->flashdata('notif').'</div>';} 
        ?>
    </section><br>
    </div>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Data Penyakit Tahun <?php echo($tahun); ?></h3>
        </div>
        <div class="box-body">
          <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th >No</th>
                  <th class="no-sort" style="width: 350px">Penyakit</th>
                  <th class="no-sort" >Jan</th>
                  <th class="no-sort" >Feb</th>
                  <th class="no-sort" >Mar</th>
                  <th class="no-sort" >Apr</th>
                  <th class="no-sort" >Mei</th>
                  <th class="no-sort" >Jun</th>
                  <th class="no-sort" >Jul</th>
                  <th class="no-sort" >Ags</th>
                  <th class="no-sort" >Sep</th>
                  <th class="no-sort" >Okt</th>
                  <th class="no-sort" >Nov</th>
                  <th class="no-sort" >Des</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    $no = 1;
                    foreach ($data_edit as $data) {
                        $penyakit = $data->penyakit;
                        $jan = $data->jan;
                        $feb = $data->feb;
                        $mar = $data->mar;
                        $apr = $data->apr;
                        $mei = $data->mei;
                        $jun = $data->jun;
                        $jul = $data->jul;
                        $ags = $data->ags;
                        $sep = $data->sep;
                        $okt = $data->okt;
                        $nov = $data->nov;
                        $des = $data->des;
                        ?>
                        <tr>
                            <td><?php echo $no++ ?></td>
                            <td><?php echo $penyakit ?></td>
                            <td><?php echo $jan ?></td>
                            <td><?php echo $feb ?></td>
                            <td><?php echo $mar ?></td>
                            <td><?php echo $apr ?></td>
                            <td><?php echo $mei ?></td>
                            <td><?php echo $jun ?></td>
                            <td><?php echo $jul ?></td>
                            <td><?php echo $ags ?></td>
                            <td><?php echo $sep ?></td>
                            <td><?php echo $okt ?></td>
                            <td><?php echo $nov ?></td>
                            <td><?php echo $des ?></td>
                        </tr>
                        <?php
                    }
                ?>
                </tbody>
              </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <!-- Footer -->
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
