 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah Data
        <small>Tambah data penyakit</small>
      </h1>
      <ol class="breadcrumb">
          <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Tambah Data</li>
      </ol>
    </section><br>
    </div>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Tambah Data Penyakit</h3>
        </div>
        <div class="box-body">
          <!-- form start -->
            <form action="<?php echo base_url("admin/doinsertdata");?>" method="post" >
              <div class="box-body">
                  <?php if($this->session->flashdata('notif')){
                        $type = $this->session->flashdata('type');
                        echo "<div id='alert' class='alert $type'>";
                        echo $this->session->flashdata('notif').'</div>';
                    } ?>
                  <div class="row">   
                    <div class="col-md-12">
                      <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-1 control-label">Tahun</label>
                            <div class="col-sm-11">
                              <input type="number" name="tahun" class="form-control" placeholder="Masukan Tahun" autofocus>
                            </div>
                        </div>
                      </div>
                    </div>   
                  </div>
                  <div class="row table-responsive">
                      <table class="table">
                        <tr>
                          <th>No</th>
                          <th>Penyakit</th>
                          <th>Jan</th>
                          <th>Feb</th>
                          <th>Mar</th>
                          <th>Apr</th>
                          <th>Mei</th>
                          <th>Jun</th>
                          <th>Jul</th>
                          <th>Ags</th>
                          <th>Sep</th>
                          <th>Okt</th>
                          <th>Nov</th>
                          <th>Des</th>                          
                        </tr> 
                        <?php
                          $i = 1;
                          foreach ($daftar_penyakit as $data) {
                            echo "
                          <tr>
                          <td>$i</td>
                          <td> 
                            $data->nama_penyakit
                            <input type='hidden' name='penyakit[$i]' value='$data->nama_penyakit'>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='jan[$i]' class='form-control' placeholder='jan'>
                            </div>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='feb[$i]' class='form-control' placeholder='feb'>
                            </div>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='mar[$i]' class='form-control' placeholder='mar'>
                            </div>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='apr[$i]' class='form-control' placeholder='apr'>
                            </div>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='mei[$i]' class='form-control' placeholder='mei'>
                            </div>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='jun[$i]' class='form-control' placeholder='jun'>
                            </div>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='jul[$i]' class='form-control' placeholder='jul'>
                            </div>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='ags[$i]' class='form-control' placeholder='ags'>
                            </div>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='sep[$i]' class='form-control' placeholder='sep'>
                            </div>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='okt[$i]' class='form-control' placeholder='okt'>
                             </div>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='nov[$i]' class='form-control' placeholder='nov'>
                             </div>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='des[$i]' class='form-control' placeholder='des'>
                             </div>
                          </td>
                          </tr>";
                            $i++;
                          }
                        ?>                          
                        
                      </table>
                  </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button onclick="return confirm('Apakah data yg diisikan sudah benar? jika sudah benar, data yang disimpan tidak dapat diubah');" type="submit" class="btn btn-primary">Simpan</button>
                <button type="reset" class="btn btn-danger">Reset</button>
              </div>
            </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
