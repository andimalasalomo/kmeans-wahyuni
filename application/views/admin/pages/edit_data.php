 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manajemen Data
        <small>Manajemen data penyakit</small>
      </h1>
      <ol class="breadcrumb">
          <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="data">Manajemen Data</a></li>
          <li class="active">Ubah Data</li>
      </ol>
    </section><br>
    </div>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Ubah Data Penyakit</h3>
        </div>
        <div class="box-body">
          <!-- form start -->
           <form action="<?php echo base_url("admin/doeditdata");?>" method="post" >
              <div class="box-body">
                  <?php if($this->session->flashdata('notif')){
                        $type = $this->session->flashdata('type');
                        echo "<div id='alert' class='alert $type'>";
                        echo $this->session->flashdata('notif').'</div>';
                    } ?>
                  <div class="row">   
                    <div class="col-md-12">
                      <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-1 control-label">Tahun</label>
                            <div class="col-sm-11">
                              <input value="<?php echo($tahun);  ?>" type="number" name="tahun" class="form-control" placeholder="Masukan Tahun">
                            </div>
                        </div>
                      </div>
                    </div>   
                  </div>
                  <div class="row">
                      <table class="table table-responsive">
                        <tr>
                          <th>No</th>
                          <th>Penyakit</th>
                          <th>Jan</th>
                          <th>Feb</th>
                          <th>Mar</th>
                          <th>Apr</th>
                          <th>Mei</th>
                          <th>Jun</th>
                          <th>Jul</th>
                          <th>Ags</th>
                          <th>Sep</th>
                          <th>Okt</th>
                          <th>Nov</th>
                          <th>Des</th>                          
                        </tr> 
                        <?php
                          $i = 1;
                          foreach ($data_edit as $data) {
                            echo "
                          <tr>
                          <td>$i</td>
                          <td> 
                            $data->penyakit
                            <input type='hidden' name='penyakit[$i]' value='$data->penyakit'>
                            <input type='hidden' name='id_data[$i]' value='$data->id_data'>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='jan[$i]' value='$data->jan' class='form-control' placeholder='jan'>
                            </div>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='feb[$i]' value='$data->feb' class='form-control' placeholder='feb'>
                            </div>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='mar[$i]' value='$data->mar' class='form-control' placeholder='mar'>
                            </div>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='apr[$i]' value='$data->apr' class='form-control' placeholder='apr'>
                            </div>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='mei[$i]' value='$data->mei' class='form-control' placeholder='mei'>
                            </div>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='jun[$i]' value='$data->jun' class='form-control' placeholder='jun'>
                            </div>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='jul[$i]' value='$data->jul' class='form-control' placeholder='jul'>
                            </div>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='ags[$i]' value='$data->ags' class='form-control' placeholder='ags'>
                            </div>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='sep[$i]' value='$data->sep' class='form-control' placeholder='sep'>
                            </div>
                          </td>
                          <td>
                            <div class='form-group'>
                              <input type='number' name='okt[$i]' value='$data->okt' class='form-control' placeholder='okt'>
                             </div>
                          </td>
                           <td>
                            <div class='form-group'>
                              <input type='number' name='nov[$i]' value='$data->nov' class='form-control' placeholder='nov'>
                             </div>
                          </td>
                           <td>
                            <div class='form-group'>
                              <input type='number' name='des[$i]' value='$data->des' class='form-control' placeholder='des'>
                             </div>
                          </td>
                          </tr>";
                            $i++;
                          }
                        ?>                          
                        
                      </table>
                  </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="data" class="btn btn-danger">Batal</a>
              </div>
            </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
