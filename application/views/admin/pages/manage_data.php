 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manajemen Data
        <small>Manajemen data penyakit</small>
      </h1>
      <ol class="breadcrumb">
          <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Manajemen Data</li>
      </ol>
        <?php if($this->session->flashdata('notif')){
            $type = $this->session->flashdata('type');
            echo "<br><div id='alert' class='alert $type'>";
            echo $this->session->flashdata('notif').'</div>';} 
        ?>
    </section><br>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Data Penyakit</h3>
        </div>
        <div class="box-body">
          <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th style="width: 10px">No</th>                
                  <th class="no-sort" >Tahun</th>
                  <th style="width: 600px" class="no-sort">Pilihan</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    $no = 1;
                    foreach ($data_penyakit as $data) {                        
                        $tahun = $data->tahun;
                        ?>
                        <tr>
                            <td><?php echo $no++ ?></td>
                            <td><?php echo $tahun ?></td>
                            <td>
                              <div class="row">
                                <div class="col-md-2">
                                  <form action="<?php echo base_url('admin/view'); ?>" method="post">
                                    <input type="hidden" name="tahun" value="<?php echo $tahun; ?>">
                                    <button type="submit" name="confirm" class="btn btn-md btn-warning btn-block"><i class="fa fa-eye"></i> Lihat</button>
                                </form>
                                </div>  
                                <!-- <div class="col-md-2">
                                <form action="<?php echo base_url('admin/edit'); ?>" method="post">
                                    <input type="hidden" name="tahun" value="<?php echo $tahun; ?>">
                                    <button type="submit" name="confirm" value="confirm" class="btn btn-block btn-md btn-primary"><i class="fa fa-edit"></i> Ubah</button>
                                </form>
                                </div> -->
                                <div class="col-md-2">
                                <form action="" method="post" onsubmit="return confirm('Yakin menghapus data?');">
                                    <input type="hidden" name="tahun" value="<?php echo $tahun; ?>">
                                    <button type="submit" name="deleteData" value="deleteData" class="btn btn-block btn-md btn-danger"></i><i class="fa fa-trash"></i> Hapus</button>
                                </form>
                                </div>
                              </div>
                            </td>
                        </tr>
                      <?php
                    }
                ?>
              </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <!-- Footer -->
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    </div>
  </div>
  <!-- /.content-wrapper -->
